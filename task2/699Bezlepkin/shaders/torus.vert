#version 330

//стандартные матрицы для преобразования координат
uniform mat4 modelMatrix; //из локальной в мировую
uniform mat4 viewMatrix; //из мировой в систему координат камеры
uniform mat4 projectionMatrix; //из системы координат камеры в усеченные координаты
//матрица для преобразования нормалей из локальной системы координат в систему координат камеры
uniform mat3 normalToCameraMatrix;

layout(location = 0) in vec3 vertexCoords;
layout(location = 1) in vec3 vertexNormal;
out vec3 v_norm;
/*out vec3 normalCamSpace; //нормаль в системе координат камеры
out vec4 lightPosCamSpace; //положение источника света в системе координат камеры
out vec4 posCamSpace; //координаты вершины в системе координат камеры
*/
void main()
{/*
   posCamSpace = viewMatrix * modelMatrix * vec4(vertexPosition, 1.0); //преобразование координат вершины в систему координат камеры
   normalCamSpace = normalize(normalToCameraMatrix * vertexNormal); //преобразование нормали в систему координат камер
   lightPosCamSpace = viewMatrix * vec4(light.pos, 1.0); //преобразование положения источника света в систему координат камеры

   */
   gl_Position = vec4(vertexCoords, 1.0);
   v_norm = vertexNormal;
}
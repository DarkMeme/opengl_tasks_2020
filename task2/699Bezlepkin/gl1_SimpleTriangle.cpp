#define GLM_ENABLE_EXPERIMENTAL
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/vec4.hpp>
#include <glm/vec3.hpp>
#include <glm/vec2.hpp>

#include <glm/gtx/rotate_vector.hpp>
#include <iostream>
#include <iomanip>
#include <vector>
#include <glm/geometric.hpp>

using glm::vec4;
using glm::vec2;
using glm::vec3;
using glm::mat4;
//Функция обратного вызова для обработки событий клавиатуры
void myKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE)
    {
        //Если нажата клавиша ESCAPE, то закрываем окно
        glfwSetWindowShouldClose(window, GL_TRUE);
    }
}
vec3 vp(vec3 a, vec3 b) {
    return {a[1] * b[2] - a[2] * b[1], -a[0] * b[2] + a[2] * b[0], a[0] * b[1] - a[1] * b[0]};
}
vec3 getNorm(vec3 v1, vec3 v2, vec3 v3) {
    v1 *= 100.0f;
    v2 *= 100.0f;
    v3 *= 100.0f;

    return glm::normalize(vp((v3 - v1), (v2 - v1)));
}

struct VertexData {
    vec4 vertexCoords;
    vec4 vectorNorm;
};
class FloatBuffer {
    float* buffer;
    int size;
    int capacity;
public:
    FloatBuffer(){}
    FloatBuffer(int uShift, int vShift) {
        buffer = nullptr;
        reconfigure(uShift, vShift);
    }
    void reconfigure(int u, int v) {
        if (buffer != nullptr)
            delete buffer;
        size = 0;
        // numberOfTriags * vertsPerTriag * targetSpaceDims3D * attr3DCount + numberOfTriags * vertsPerTriag * targetSpaceDims2D * attr2DCount
        capacity = (u * v* 2) * 3 * 3 * 4;
        buffer = (float*)malloc(capacity* sizeof(float));
    }
    float& operator[](int ind) {
        return buffer[ind];
    }
    void Append(float value) {
        buffer[size++] = value;
    }
    void Append(vec2 vec) {
        Append(vec[0] / 100.0);
        Append(vec[1] / 100.0);
    }
    void Append(vec3 vec) {
        Append(vec[0]);
        Append(vec[1]);
        Append(vec[2]);
    }
    void Append(vec4 vec) {
        Append(vec[0]);
        Append(vec[1]);
        Append(vec[2]);
    }
    void Append(VertexData v1, VertexData v2, VertexData v3) {
        vec3 normTorus = getNorm(v1.vertexCoords, v2.vertexCoords, v3.vertexCoords);

        Append(v1.vertexCoords);
        Append(normTorus);
        Append(v2.vertexCoords);
        Append(normTorus);
        Append(v3.vertexCoords);
        Append(normTorus);
    }
    float* ExposeBuffer() {
        return buffer;
    }
    int GetSize() const {
        return size;
    }
};

const float R1 = 0.7f, R2 = 0.1f;
int gridSize = 20;
const int gridSizeMax = 100, gridSizeMin = 5;
FloatBuffer *vertexBuffer;

VertexData Rotate(mat4 rotationMat, VertexData vertexData) {
    return {
            rotationMat * vertexData.vertexCoords,
            rotationMat * vertexData.vectorNorm
    };
}

VertexData coordsByGrid(int uShift, int vShift, int i, int j) {
    i %= uShift;
    j %= vShift;
    float alpha = 2 * M_PI * i / uShift;
    float betha = 2 * M_PI * j / vShift;

    vec4 Vcoords((R1 + R2*cos(betha))*sin(alpha), (R1 + R2*cos(betha))*cos(alpha) , R2*sin(betha), 1.0f);
    vec4 Ncoords(1.0f, 1.0f, 1.0f,1.0f);
    VertexData ans;
    ans.vertexCoords = Vcoords;
    ans.vectorNorm = Ncoords;
    return ans;
}

void TorusBuffer(FloatBuffer* buffer, int uShift, int vShift) {
    buffer->reconfigure(uShift, vShift);

    mat4 rotationMat(1.0);
    rotationMat[0][0] = rotationMat[1][1] = rotationMat[2][2] = rotationMat[3][3] = 1.0;
    rotationMat = glm::rotate(rotationMat, -(float)M_PI / 1 * (float)1.3, vec3(0.0f, 1.0f, 0.0f));
    rotationMat = glm::rotate(rotationMat, (float)M_PI / 1, vec3(1.0f, 0.0f, 0.0f));
    rotationMat = glm::rotate(rotationMat, (float)M_PI / 1, vec3(0.0f, 0.0f, 1.0f));
    for (int i = 0; i < uShift; ++i) {
        for (int j = 0; j < vShift; ++j) {
            VertexData LD = coordsByGrid(uShift, vShift, i, j);
            VertexData RD = coordsByGrid(uShift, vShift, i + 1, j);
            VertexData LU = coordsByGrid(uShift, vShift, i, j + 1);
            VertexData RU = coordsByGrid(uShift, vShift, i + 1, j + 1);
            buffer->Append(Rotate(rotationMat, LU), Rotate(rotationMat, LD), Rotate(rotationMat, RU));
            buffer->Append(Rotate(rotationMat, LD), Rotate(rotationMat, RD), Rotate(rotationMat, RU));
        }
    }
}

void reconfigureBuffer() {
    TorusBuffer(vertexBuffer, gridSize, gridSize);
    }


#include "../../common/ShaderProgram.hpp"
#include <../../common/Application.hpp>

class SampleApplication: public Application {
public:
    ShaderProgramPtr _shader;
    FloatBuffer *_vBuffer;
    float* _points;
    GLuint _vao;
    GLuint _vbo;
    //Координаты источника света
    float _lr = 5.0;
    float _phi = 0.0;
    float _theta = glm::pi<float>() * 0.25f;

    //Параметры источника света
    glm::vec3 _lightAmbientColor;
    glm::vec3 _lightColor;

    //Параметры материала
    glm::vec3 _bunnyDiffuseColor;
    glm::vec3 _bunnySpecularColor;
    float _diffuseFraction = 0.0f;
    float _roughnessValue = 0.3f;

    int _gridSize = 20;
    int _gridSizeMax = 300;
    int _gridSizeMin = 5;
    int _gridSizeStep = 5;
    float _F0 = 0.8f;
    void _reconfigureBuffer() {
        TorusBuffer(_vBuffer, _gridSize, _gridSize);
    }
    void handleKey(int key, int scancode, int action, int mods) override {
        if (action == GLFW_PRESS)
        {
            if (key == GLFW_KEY_ESCAPE)
            {
                glfwSetWindowShouldClose(_window, GL_TRUE);
            } else if (key == GLFW_KEY_MINUS) {
                if (_gridSize > _gridSizeMin) {
                    std::cout << "quality decreased " << _gridSize << std::endl;
                    _gridSize -= _gridSizeStep;
                    _reconfigureBuffer();
                }
            } else if (key == GLFW_KEY_EQUAL)
            {
                if (_gridSize < _gridSizeMax) {
                    std::cout << "quality increased" << _gridSize << std::endl;
                    _gridSize += _gridSizeStep;
                    _reconfigureBuffer();
                }
            }
        }

        _cameraMover->handleKey(_window, key, scancode, action, mods);
    }
    void makeScene() override {
        //std::cout<<"SSSSSSSS\n";
        Application::makeScene();
        std::cout<<"m1s\n";
        _shader = std::make_shared<ShaderProgram>("C:/Users/Matvey Bezlepkin/Documents/GitProjects/opengl_tasks_2020/task2/699Bezlepkin/shaders/torus.vert", "C:/Users/Matvey Bezlepkin/Documents/GitProjects/opengl_tasks_2020/task2/699Bezlepkin/shaders/torus.frag");
        //Создание и загрузка мешей
        std::cout<<"m2s\n";
        _vBuffer = new FloatBuffer(0, 0);
        _reconfigureBuffer();
        _points =_vBuffer->ExposeBuffer();
        std::cout<<"m3s\n";
        //Создаем буфер VertexBufferObject для хранения координат на видеокарте

        std::cout<<"m3.0s\n";
        glGenBuffers(1, &_vbo);

        std::cout<<"m3.1s\n";
        //Делаем этот буфер текущим
        glBindBuffer(GL_ARRAY_BUFFER, _vbo);

        std::cout<<"m3.2s\n";
        //Копируем содержимое массива в буфер на видеокарте
        glBufferData(GL_ARRAY_BUFFER, _vBuffer->GetSize() * sizeof(float), _points, GL_STATIC_DRAW);

        //=========================================================

        std::cout<<"m4s\n";
        //Создаем объект VertexArrayObject для хранения настроек полигональной модели
        glGenVertexArrays(1, &_vao);

        //Делаем этот объект текущим
        glBindVertexArray(_vao);

        //Делаем буфер с координатами текущим
        glBindBuffer(GL_ARRAY_BUFFER, _vbo);

        //Включаем 0й вершинный атрибут
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);

        std::cout<<"m5s\n";
        //Устанавливаем настройки:
        //0й атрибут,
        //3 компоненты типа GL_FLOAT,
        //не нужно нормализовать,
        //0 - значения расположены в массиве впритык,
        //0 - сдвиг от начала
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GL_FLOAT), reinterpret_cast<void*>(0));
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GL_FLOAT), reinterpret_cast<void*>(3* sizeof(GL_FLOAT)));

        glBindVertexArray(0);

      //  std::cout<<"$$$$$$$$\n";
    }
    void updateGUI() override {
        //std::cout<<"GGGGGGGG\n";
        Application::updateGUI();
        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("Matvey Bezlepkin Torus", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Light"))
            {
                ImGui::ColorEdit3("ambient", glm::value_ptr(_lightAmbientColor));
                ImGui::ColorEdit3("color", glm::value_ptr(_lightColor));

                ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
            }

           /* if (ImGui::CollapsingHeader("Rabbit material"))
            {
                ImGui::ColorEdit3("mat diffuse", glm::value_ptr(_bunnyDiffuseColor));
                ImGui::ColorEdit3("mat specular", glm::value_ptr(_bunnySpecularColor));

                ImGui::SliderFloat("diffuse fraction", &_diffuseFraction, 0.01f, 1.0f);
                ImGui::SliderFloat("roughness", &_roughnessValue, 0.01f, 1.0f);
                ImGui::SliderFloat("F0", &_F0, 0.01f, 1.0f);
            }*/
        }
        ImGui::End();
    }
    void draw() {
       // std::cout << "FFFFFFFF\n";
        //Получаем размеры экрана (окна)
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);
        glfwGetFramebufferSize(_window, &width, &height);

        //Устанавливаем порт вывода на весь экран (окно)
        glViewport(0, 0, width, height);


        _reconfigureBuffer();

        //Очищаем порт вывода (буфер цвета и буфер глубины)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        _shader->use();
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        _shader->setMat4Uniform("modelMatrix", glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));

        glBindVertexArray(_vao);
        glBufferData(GL_ARRAY_BUFFER, _vBuffer->GetSize() * sizeof(float), _vBuffer->ExposeBuffer(), GL_STATIC_DRAW);

        glDrawArrays(GL_TRIANGLES, 0, _vBuffer->GetSize()  );
       // std::cout << "##########\n";
    }
};
int main()
{
    /*
    //Инициализируем библиотеку GLFW
    if (!glfwInit())
    {
        std::cerr << "ERROR: could not start GLFW3\n";
        exit(1);
    }

    //Устанавливаем параметры создания графического контекста
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    //Создаем графический контекст (окно)
    GLFWwindow* window = glfwCreateWindow(800, 600, "MIPT OpenGL demos", nullptr, nullptr);
    if (!window)
    {
        std::cerr << "ERROR: could not open window with GLFW3\n";
        glfwTerminate();
        exit(1);
    }

    //Делаем этот контекст текущим
    glfwMakeContextCurrent(window);

    //Устанавливаем функцию обратного вызова для обработки событий клавиатуры
    glfwSetKeyCallback(window, myKeyCallback);

    //Инициализируем библиотеку GLEW
    glewExperimental = GL_TRUE;
    glewInit();

    //=========================================================

    //Координаты вершин треугольника

    vertexBuffer = new FloatBuffer(0, 0);
    reconfigureBuffer();
    float* points =vertexBuffer->ExposeBuffer();
    //Создаем буфер VertexBufferObject для хранения координат на видеокарте
    GLuint vbo;
    glGenBuffers(1, &vbo);
     //Делаем этот буфер текущим
    /*glBindBuffer(GL_ARRAY_BUFFER, vbo);

    //Копируем содержимое массива в буфер на видеокарте
    glBufferData(GL_ARRAY_BUFFER, vertexBuffer->GetSize() * sizeof(float), points, GL_STATIC_DRAW);

    //=========================================================

    //Создаем объект VertexArrayObject для хранения настроек полигональной модели
    GLuint vao;
    glGenVertexArrays(1, &vao);

    //Делаем этот объект текущим
    glBindVertexArray(vao);

    //Делаем буфер с координатами текущим
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    //Включаем 0й вершинный атрибут
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    //Устанавливаем настройки:
    //0й атрибут,
    //3 компоненты типа GL_FLOAT,
    //не нужно нормализовать,
    //0 - значения расположены в массиве впритык,
    //0 - сдвиг от начала
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GL_FLOAT), reinterpret_cast<void*>(0));
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GL_FLOAT), reinterpret_cast<void*>(3* sizeof(GL_FLOAT)));

    glBindVertexArray(0);

    //=========================================================

    //Вершинный шейдер
   /* const char* vertexShaderText =
            "#version 330\n"
            "layout(location = 0) in vec3 vertexCoords;\n"
            "layout(location = 1) in vec3 vertexNormal;\n"
            "out vec3 v_norm;\n"
            "void main()\n"
            "{\n"
            "   gl_Position = vec4(vertexCoords, 1.0);\n"
            "   v_norm = vertexNormal;\n"
            "}\n";

    //Создаем шейдерный объект
    GLuint vs = glCreateShader(GL_VERTEX_SHADER);

    //Передаем в шейдерный объект текст шейдера
    glShaderSource(vs, 1, &vertexShaderText, nullptr);

    //Компилируем шейдер
    glCompileShader(vs);

    //Проверяем ошибки компиляции
    int status = -1;
    glGetShaderiv(vs, GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE)
    {
        GLint errorLength;
        glGetShaderiv(vs, GL_INFO_LOG_LENGTH, &errorLength);

        std::vector<char> errorMessage;
        errorMessage.resize(errorLength);

        glGetShaderInfoLog(vs, errorLength, 0, errorMessage.data());

        std::cerr << "Failed to compile the shader:\n" << errorMessage.data() << std::endl;

        exit(1);
    }

    //=========================================================

    //Фрагментный шейдер
    const char* fragmentShaderText =
            "#version 330\n"

            "out vec4 fragColor;\n"

            "in vec3 v_norm;\n"

            "void main()\n"
            "{\n"
            "    fragColor = vec4(v_norm*0.5 + 0.5, 0.0);\n"
            "}\n";

    //Создаем шейдерный объект
    GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);

    //Передаем в шейдерный объект текст шейдера
    glShaderSource(fs, 1, &fragmentShaderText, nullptr);

    //Компилируем шейдер
    glCompileShader(fs);

    //Проверяем ошибки компиляции
    status = -1;
    glGetShaderiv(fs, GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE)
    {
        GLint errorLength;
        glGetShaderiv(fs, GL_INFO_LOG_LENGTH, &errorLength);

        std::vector<char> errorMessage;
        errorMessage.resize(errorLength);

        glGetShaderInfoLog(fs, errorLength, 0, errorMessage.data());

        std::cerr << "Failed to compile the shader:\n" << errorMessage.data() << std::endl;

        exit(1);
    }

    //=========================================================

    //Создаем шейдерную программу
    GLuint program = glCreateProgram();

    //Прикрепляем шейдерные объекты
    glAttachShader(program, vs);
    glAttachShader(program, fs);
*/
    /* auto programTorus = std::make_shared<ShaderProgram>("C:/Users/Matvey Bezlepkin/Documents/GitProjects/opengl_tasks_2020/task2/699Bezlepkin/shaders/torus.vert", "C:/Users/Matvey Bezlepkin/Documents/GitProjects/opengl_tasks_2020/task2/699Bezlepkin/shaders/torus.frag");
    //Создаем шейдерную программу
    auto& program = programTorus->_programId;

    //Линкуем программу
    glLinkProgram(program);

    //Проверяем ошибки линковки
    int status = -1;
    glGetProgramiv(program, GL_LINK_STATUS, &status);
    if (status != GL_TRUE)
    {
        GLint errorLength;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &errorLength);

        std::vector<char> errorMessage;
        errorMessage.resize(errorLength);

        glGetProgramInfoLog(program, errorLength, 0, errorMessage.data());

        std::cerr << "Failed to link the program:\n" << errorMessage.data() << std::endl;

        exit(1);
    }

    //Линкуем программу
    glLinkProgram(program);
    GLint loc = glGetUniformLocation(program, "shiftVec");
    std::cout << loc;
    if (loc != -1)
    {
        float val = 0.40;
        glUniform1f(loc, val);
    }
    //Проверяем ошибки линковки
    status = -1;
    glGetProgramiv(program, GL_LINK_STATUS, &status);
    if (status != GL_TRUE)
    {
        GLint errorLength;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &errorLength);

        std::vector<char> errorMessage;
        errorMessage.resize(errorLength);

        glGetProgramInfoLog(program, errorLength, 0, errorMessage.data());

        std::cerr << "Failed to link the program:\n" << errorMessage.data() << std::endl;

        exit(1);
    }

    //=========================================================
    std:: cout<< "%%%";
    //Цикл рендеринга (пока окно не закрыто)
    while (!glfwWindowShouldClose(window))
    {
        //Проверяем события ввода (здесь вызывается функция обратного вызова для обработки событий клавиатуры)
        glfwPollEvents();

        //Получаем размеры экрана (окна)
        int width, height;
        glfwGetFramebufferSize(window, &width, &height);

        //Устанавливаем порт вывода на весь экран (окно)
        glViewport(0, 0, width, height);

        //Очищаем порт вывода (буфер цвета и буфер глубины)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдерную программу
        glUseProgram(program);

        //Подключаем VertexArrayObject с настойками полигональной модели
        glBindVertexArray(vao);

        //Рисуем полигональную модель (состоит из треугольников, сдвиг 0, количество вершин 3)
        glDrawArrays(GL_TRIANGLES, 0, vertexBuffer->GetSize()  );

        float val = 0.40;
        //glUniform1f(loc, val);
        glfwSwapBuffers(window); //Переключаем передний и задний буферы
    }

    //Удаляем созданные объекты OpenGL
    glDeleteProgram(program);
   // glDeleteShader(vs);
   // glDeleteShader(fs);
    glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(1, &vbo);

    glfwTerminate();
    */
    SampleApplication app;
    app.start();

    return 0;
}